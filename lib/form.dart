import 'package:flutter/material.dart';


class Myform extends StatefulWidget {
  @override
  _MyformState createState() => _MyformState();
}

class _MyformState extends State<Myform> {


  var _formkey = GlobalKey<FormState>();
  var student_class = [
    'I',
    'II',
    'III',
    'IV',
    'V',
    'VI',
    'VII',
    'VIII',
    'IX',
    'X',
    'XI',
    'XII'
  ];
  var profession_clg = [
    'Teacher',
    'Accountant',
    'Librarian',
    'Sweeper',
  ];
  var current_Student_class = 'class';
  var current_profession = 'profession';

  void initState() {
    current_Student_class = student_class[0];
    current_profession = profession_clg[0];
    super.initState();
  }

  @override
  int group = 1;
  bool isEarning = true;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Submition Form",
          style: TextStyle(
            fontSize: 25.0,
            fontFamily: 'Serif',
            fontWeight: FontWeight.w400,
          ),
        ),
        centerTitle: true,
        leading: Icon(Icons.people),
        backgroundColor: Colors.black87,
      ),

      body: Container(
        color: Colors.black45,
        child: Form(


          key: _formkey,

          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: TextFormField(

                    maxLength: 30,
                    validator: validateName,
                    decoration: InputDecoration(
                        fillColor: Colors.white70,
                        filled: true,
                        prefixIcon: Icon(Icons.person),
                        labelStyle: TextStyle(fontSize: 18.0),
                        labelText: 'Name',
                        hintText: 'ex-Rahul',
                        errorStyle: TextStyle(
                          fontSize: 17.0,
                          color: Colors.black87,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0))),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: TextFormField(

                    maxLength: 50,
                    validator: validatePassword,
                    decoration: InputDecoration(
                      fillColor: Colors.white70,
                      filled: true,
                      labelText: 'Password',
                      prefixIcon: Icon(Icons.keyboard),
                      labelStyle: TextStyle(
                        fontSize: 18.0,
                      ),
                      hintText: 'Enter Password',
                      errorStyle: TextStyle(
                        fontSize: 17.0,
                        color: Colors.black87,
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8.0)),
                    ),
                    obscureText: true,
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "Student",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Radio(
                        value: 1,
                        groupValue: group,
                        onChanged: (T) {
                          setState(() {
                            group = T;
                            isEarning = true;
                          });
                        },
                        activeColor: Color(hexcolor('73264d')),
                      ),
                    ),
                    Text("Professional", style: TextStyle(fontSize: 20.0)),
                    Padding(
                      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Radio(
                        value: 2,
                        groupValue: group,
                        onChanged: (T) {
                          setState(() {
                            group = T;

                            isEarning = false;
                          });
                        },
                        activeColor: Color(hexcolor('73264d')),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    isEarning
                        ? Padding(
                      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Container(
                        width: 380,
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: Colors.black87,
                          ),
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                          color: Color(hexcolor('#f2f2f2')),
                        ),
                        child: Center(
                          child: DropdownButton<String>(
                            items: student_class.map((String dropitem) {
                              return DropdownMenuItem<String>(
                                  value: dropitem, child: Text(dropitem));
                            }).toList(),
                            onChanged: (String newValue) {
                              setState(() {
                                current_Student_class = newValue;
                              });
                            },
                            value: this.current_Student_class,
                          ),
                        ),
                      ),
                    )
                        : Padding(
                      padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                      child: Container(
                          width: 380,
                          height: 50,
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: Colors.black87,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(18.0),
                            ),
                            color: Color(hexcolor('#ffffff')),
                          ),
                          child: Center(
                            child: DropdownButton<String>(
                              items: profession_clg.map((String dropitem) {
                                return DropdownMenuItem<String>(
                                    value: dropitem, child: Text(dropitem));
                              }).toList(),
                              onChanged: (String newValue) {
                                setState(() {
                                  current_profession = newValue;
                                });
                              },
                              value: this.current_profession,
                            ),
                          )),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
                  child: TextFormField(


                    keyboardType: TextInputType.number,
                    validator: validateMobile,
                    decoration: InputDecoration(
                        fillColor: Colors.white70,
                        filled: true,
                        prefixIcon: Icon(Icons.person),
                        labelStyle: TextStyle(fontSize: 18.0),
                        labelText: 'Phone No.',
                        hintText: '+1',
                        errorStyle: TextStyle(
                          fontSize: 17.0,
                          color: Colors.black87,
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8.0))),
                  ),
                ),
                Center(
                  child: RaisedButton(
                    child: Text("Submit",
                      style: TextStyle(
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                    onPressed:
                    ValidationForm,
                    /* FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
    currentFocus.unfocus();
    }*/


                    color: Colors.black87,
                  ),
                )
              ],
            ),
          ),

        ),
      ),
    );
  }

  hexcolor(String colorhexa) {
    String colornew = '0xFF' + colorhexa;
    colornew = colornew.replaceAll('#', '');
    int colorint = int.parse(colornew);
    return colorint;
  }

  String validateName(String value) {
    String patt = r'(^[a-zA-Z 0-9 \_]*$)';
    RegExp regExp = new RegExp(patt);
    if (value.length == 0) {
      return "Username is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Inavlid Username";
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    String patt = r'(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$)';
    RegExp regExp = new RegExp(patt);
    if (value.length == 0) {
      return "This field should not be empty";
    }
    else if (value.length<8) {
      return "password should greater than 8";
    }
    else if (!regExp.hasMatch(value)) {
      return "Password must have 1-uppercase,\n1-lowercase,1-spcical symobol like-@,_";
    } else {
      return null;
    }
  }

  String validateMobile(String value) {
    String patt = r'(^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$)';
    RegExp regExp = new RegExp(patt);
    if (value.length == 0) {
      return "Mobile Number  is Required";
    } else if (value.length > 15) {
      return "invalid number";
    } else if (!regExp.hasMatch(value)) {
      return "Phone number must be +1-253-222-3452";
    } else {
      return null;
    }
  }

  ValidationForm() {
    _formkey.currentState.validate();
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }
}
