import 'package:flutter/material.dart';



class CountryList extends StatefulWidget {
  @override
  _CountryListState createState() => _CountryListState();
}

class _CountryListState extends State<CountryList> {
  @override
 Map<String,bool> countryName=
  {
    'India': false,
    'China': false,
    'Russia': false,
    'America': false,
    'Bangladesh': false,
    'Nepal': false,
    'Japan': false,
    'Iran': false,
  };
String name='Select';



  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Country List'),
        centerTitle: true,
        backgroundColor: Colors.black87,

      ),

          body:Container(
            child:Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  child: Text(
                    '$name',
                    style: TextStyle(
                      fontSize: 30.0,
                    ),
                  ),

                ),
                Expanded(
                  child: ListView(
                    children:countryName.keys.map((String key)
                    {
                      return CheckboxListTile(
                        title: Text(key),
                        value:countryName[key],
                        onChanged:(bool val)
                        {

                            onSelect(key,val);

                        },
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          )

          );



    
  }
  onSelect(String k,bool value)
  {
    setState((){

        countryName[k] = value;
        debugPrint(k);

        for(String key in countryName.keys)
        {
          if(key != k)
            {
              countryName[key] == false;
                 
            }


        }


      name = k;
    } );

  }
}
