import 'package:flutter/material.dart';

class SecondRoute extends StatefulWidget {
  @override
  _SecondRouteState createState() => _SecondRouteState();
}

class _SecondRouteState extends State<SecondRoute> {
  @override
  var _currencies = ['Rupees', 'Dollars', 'Pounds'];
  final double _minimumPadding = 5.0;
  var _dol = '';
  var _disp = '';
  var _formkey = GlobalKey<FormState>();
  TextEditingController pri = TextEditingController();
  TextEditingController roi = TextEditingController();
  TextEditingController time = TextEditingController();

  @override
  void initState() {
    _dol = _currencies[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//			resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('Simple Interest Calculator'),
        backgroundColor: Colors.black87,
        centerTitle: true,
        leading: Icon(Icons.attach_money),
      ),
      backgroundColor: Colors.white70,

      body: Form(
        key: _formkey,
        // margin: EdgeInsets.all(_minimumPadding * 2),
        child: Padding(
            padding: EdgeInsets.all(_minimumPadding * 2),
            child: ListView(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: TextFormField(
                      controller: pri,
                      keyboardType: TextInputType.number,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter principal amount';
                        }
                      },
                      decoration: InputDecoration(
                          fillColor: Colors.white70,
                          filled: true,
                          prefixIcon: Icon(Icons.monetization_on),
                          labelStyle: TextStyle(fontSize: 18.0),
                          labelText: 'Principal',
                          hintText: 'Enter Principal e.g. 12000',
                          errorStyle: TextStyle(
                            fontSize: 17.0,
                            color: Colors.black87,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0))),
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: TextFormField(
                      controller: roi,
                      keyboardType: TextInputType.number,
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter rate of interest';
                        }
                      },
                      decoration: InputDecoration(
                          fillColor: Colors.white70,
                          filled: true,
                          labelStyle: TextStyle(fontSize: 18.0),
                          labelText: 'Rate of Interest',
                          hintText: 'In percent',
                          errorStyle: TextStyle(
                            fontSize: 17.0,
                            color: Colors.black87,
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(8.0))),
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        top: _minimumPadding, bottom: _minimumPadding),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: TextFormField(
                              controller: time,
                              keyboardType: TextInputType.number,
                              validator: (String value) {
                                if (value.isEmpty) {
                                  return 'Please enter year';
                                }
                              },
                              decoration: InputDecoration(
                                  fillColor: Colors.white70,
                                  filled: true,
                                  prefixIcon: Icon(Icons.calendar_today),
                                  labelStyle: TextStyle(fontSize: 18.0),
                                  labelText: 'Time',
                                  hintText: 'Time in years',
                                  errorStyle: TextStyle(
                                    fontSize: 17.0,
                                    color: Colors.black87,
                                  ),
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                          8.0))),
                            )),
                        Container(
                          width: _minimumPadding * 5,
                        ),
                        Expanded(
                            child: DropdownButton<String>(
                              items: _currencies.map((String value) {
                                return DropdownMenuItem<String>(
                                    value: value, child: Text(value));
                              }).toList(),
                              onChanged: (String newValueSelected) {
                                setState(() {
                                  print(newValueSelected);
                                  _dol = newValueSelected;
                                });
                              },
                              value: this._dol,
                            ))
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        bottom: _minimumPadding, top: _minimumPadding),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: RaisedButton(
                              child: Text('Calculate'),
                              elevation: 8.0,
                              color: Colors.indigo[300],
                              onPressed: () {
                                setState(() {
                                  if (_formkey.currentState.validate()) {
                                    _disp = _calculate();
                                  }
                                });
                              }),
                        ),
                        Expanded(
                          child: FlatButton(
                            child: Text('Reset'),
                            color: Colors.blueGrey,
                            onPressed: () {
                              setState(() {
                                _reset();
                              });
                            },
                          ),
                        ),
                      ],
                    )),
                Padding(
                  padding: EdgeInsets.all(_minimumPadding * 2),
                  child: Text(
                    "$_disp",
                    style: TextStyle(fontSize: 18.0),
                  ),
                )
              ],
            )),
      ),
    );
  }

  _calculate() {
    double prii = double.parse(pri.text);
    double roii = double.parse(roi.text);
    double timee = double.parse(time.text);
    double total = prii + (prii * roii * timee) / 100;
    String result =
        "After $timee year , Your interest will be worth of $total $_dol ";
    return result;
  }

  _reset() {
    pri.text = '';
    roi.text = '';
    time.text = '';
    _disp = '';
    _dol = _currencies[0];
  }
}

