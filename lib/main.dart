import 'package:flutter/material.dart';
import 'list.dart';
import 'form.dart';
import 'interest.dart';
import 'video.dart';
import 'SecondRoute.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'MulitTask',
          style: TextStyle(
            fontSize: 25.0,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.black87,
        leading: Icon(Icons.table_chart),
      ),
      body: Container(
        color: Colors.black12,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding:
                        EdgeInsets.only(left: 15.0, right: 20.0, top: 180.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: SizedBox(
                        width: 180.0,
                        height: 100.0,
                        child: RaisedButton(
                          child: Text(
                            "Interest Calculator",
                            style: TextStyle(fontSize: 20.0),
                          ),
                          color: Colors.black87,
                          textColor: Colors.white,
                          elevation: 10.0,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => SIForm())));
                          },
                        ),
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 180.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: SizedBox(
                        width: 180.0,
                        height: 100.0,
                        child: RaisedButton(
                          child: Text(
                            "Submition Form",
                            style: TextStyle(fontSize: 20.0),
                          ),
                          color: Colors.black87,
                          textColor: Colors.white,
                          elevation: 10.0,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => Myform())));
                          },
                        ),
                      ),
                    )),
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                    padding:
                        EdgeInsets.only(left: 15.0, right: 20.0, top: 20.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: SizedBox(
                        width: 180.0,
                        height: 100.0,
                        child: RaisedButton(
                          child: Text(
                            "ListView",
                            style: TextStyle(fontSize: 20.0),
                          ),
                          color: Colors.black87,
                          textColor: Colors.white,
                          elevation: 10.0,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => CountryList())));
                          },
                        ),
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(20.0)),
                      child: SizedBox(
                        width: 180.0,
                        height: 100.0,
                        child: RaisedButton(
                          child: Text(
                            "Video",
                            style: TextStyle(fontSize: 20.0),
                          ),
                          color: Colors.black87,
                          textColor: Colors.white,
                          elevation: 10.0,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: ((context) => VideoPlayerScreen())));
                          },
                        ),
                      ),
                    )),
              ],
            ),

          ],
        ),
      ),
    );
  }
}
